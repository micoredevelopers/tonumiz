import './utils/menu'
import './utils/chart'
import './utils/header'
import './utils/convert'
import './utils/selectpicker'

const id = $('main').attr('id')

switch (id) {
  case 'main-page':
    require('./pages/home')
    break
}
