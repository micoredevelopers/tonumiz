$(document).ready(function () {
  $('.coin-slider').slick({
    speed: 700,
    slidesToShow: 4,
    infinite: false,
    slidesToScroll: 1,
    prevArrow: '.coin-slider-wrap .slider-arrow_prev',
    nextArrow: '.coin-slider-wrap .slider-arrow_next',
    responsive: [
      {
        breakpoint: 576,
        settings: {
          arrows: false,
          slidesToShow: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 2
        }
      },
      {
        breakpoint: 992,
        settings: {
          arrows: false,
          slidesToShow: 3
        }
      }
    ]
  })
})
