export function initChartWithTheme(theme) {
  var ctx = document.getElementById('test-chart').getContext('2d')
  
  Chart.defaults.global.defaultFontSize = 16
  Chart.defaults.global.defaultFontFamily = 'FuturaBookC'
  
  switch (theme) {
    case 'dark':
      Chart.defaults.global.defaultFontColor = '#fff'
      break
    case 'light':
      Chart.defaults.global.defaultFontColor = '#0B0B0B'
      break
  }
  
  var gradient = ctx.createLinearGradient(0, 0, 0, 400)
  gradient.addColorStop(0.2, 'rgb(216, 160, 65, 0.2)')
  gradient.addColorStop(0.99, 'rgb(216, 160, 65, 0)')
  
  new Chart(ctx, {
    type: 'line',
    data: {
      labels: ['02/09', '02/24', '02/09', '02/24', '02/24', '02/24', '02/24', '02/24'],
      datasets: [{
        backgroundColor: gradient,
        label: 'Coins',
        borderColor: '#D8A041',
        data: [1.3, 1.4, 1.45, 1.55, 1.45, 1.5, 1.38, 1.42]
      }]
    },
    options: {
      responsive: true,
      tooltips: {
        backgroundColor: '#232323'
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false
          }
        }],
        yAxes: [{
          gridLines: {
            color: '#D3D8DD',
            borderDash: [5, 5]
          },
          ticks: {
            max: 1.6,
            min: 1.2,
            stepSize: 0.1
          }
        }]
      }
    }
  })
}
