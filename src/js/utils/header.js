import { initChartWithTheme } from './chart'

function setBodyTheme(theme) {
  const $body = $('body')

  $body.attr('class', '')
  $body.addClass(`theme-${theme}`)
}

function initThemeMobile(theme) {
  $(`.header__theme_mobile.header__theme_${theme}`).addClass('d-none')
}

function setTheme(theme) {
  const defaultSrc = '/assets/images'
  const $mainImg = $('.main-section__image-wrap').find('img')

  /* Callbacks */
  setBodyTheme(theme)
  initChartWithTheme(theme)
  localStorage.setItem('theme', theme)

  if (window.innerWidth > 1199) {
    $('.header__theme').removeClass('active')
    $(`.header__theme_${theme}`).addClass('active')
  }

  $mainImg.attr('src', `${defaultSrc}/main_shape_${theme}.jpg`)
}

$(document).ready(function () {
  let lastScrollTop = 0
  const defaultTheme = 'dark'
  const storageTheme = localStorage.getItem('theme')
  const theme = storageTheme ?? defaultTheme
  
  $(window).on('scroll', function() {
    let st = window.pageYOffset || document.documentElement.scrollTop
    
    if (st > lastScrollTop) {
      $('.header').addClass('active')
    }
    
    if (st <= 0) {
      $('.header').removeClass('active')
    }
    
    lastScrollTop = st <= 0 ? 0 : st
  })

  setTheme(theme)
  initThemeMobile(theme)
  
  $('[data-toggle-theme]').on('click', function () {
    const themeAttr = $(this).attr('data-toggle-theme')

    setTheme(themeAttr)
  })

  $('.header__theme_mobile.header__theme_dark').on('click', function() {
    setTheme('dark')
    $(this).addClass('d-none')
    $('.header__theme_mobile.header__theme_light').removeClass('d-none')
  })

  $('.header__theme_mobile.header__theme_light').on('click', function() {
    setTheme('light')
    $(this).addClass('d-none')
    $('.header__theme_mobile.header__theme_dark').removeClass('d-none')
  })
})
