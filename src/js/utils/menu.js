$(document).ready(function() {
  $('.header__menu').on('click', function() {
    $(this).toggleClass('open')
    $('.global-menu').toggleClass('open')
    $('html, body').toggleClass('modal-open')
  })
  
  $('.global-menu__link').on('click', function () {
    $('.global-menu').removeClass('open')
    $('.header__menu').toggleClass('open')
    $('body, html').toggleClass('modal-open')
  })
})
